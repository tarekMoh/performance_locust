CREATE DATABASE clients_performance
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE clients_performance;

--
-- Create table `client`
--
CREATE TABLE client (
  id int NOT NULL AUTO_INCREMENT,
  uuid varchar(36) NOT NULL,
  name varchar(50) NOT NULL,
  numbers_search_allowed bit(1) NOT NULL DEFAULT b'1',
  creation_date datetime DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 77402,
AVG_ROW_LENGTH = 163,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `name` on table `client`
--
ALTER TABLE client
ADD UNIQUE INDEX name (name);

--
-- Create index `uuid` on table `client`
--
ALTER TABLE client
ADD UNIQUE INDEX uuid (uuid);

--
-- Create table `application`
--
CREATE TABLE application (
  app_id varchar(36) NOT NULL,
  app_name varchar(100) DEFAULT NULL,
  client_id int NOT NULL,
  PRIMARY KEY (app_id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 193,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE application
ADD CONSTRAINT FK_APPLICATION_CLIENT FOREIGN KEY (client_id)
REFERENCES client (id);