CREATE DATABASE numbers_performance
CHARACTER SET utf8mb4
COLLATE utf8mb4_0900_ai_ci;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE numbers_performance;

--
-- Create table `external_number`
--
CREATE TABLE external_number (
  id int NOT NULL AUTO_INCREMENT,
  number varchar(15) NOT NULL,
  service_config_id varchar(36) NOT NULL,
  shareable bit(1) NOT NULL DEFAULT b'0',
  start_date datetime NOT NULL,
  end_date datetime DEFAULT NULL,
  status varchar(50) NOT NULL,
  private_outbound_calls bit(1) NOT NULL DEFAULT b'0',
  capacity varchar(50) NOT NULL,
  note longtext DEFAULT NULL,
  voicemail_type varchar(50) DEFAULT NULL,
  inbound_messages_interface varchar(50) DEFAULT NULL,
  outbound_messages_interface varchar(50) DEFAULT NULL,
  sms_http_base_path varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 98966,
AVG_ROW_LENGTH = 236,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_EXN_NUMBER` on table `external_number`
--
ALTER TABLE external_number
ADD INDEX IX_EXN_NUMBER (number);

--
-- Create index `IX_EXN_SHAREABLE` on table `external_number`
--
ALTER TABLE external_number
ADD INDEX IX_EXN_SHAREABLE (shareable);

--
-- Create index `IX_EXN_STATUS` on table `external_number`
--
ALTER TABLE external_number
ADD INDEX IX_EXN_STATUS (status);

--
-- Create index `IX_EXN_SVC_CONFIG_ID` on table `external_number`
--
ALTER TABLE external_number
ADD INDEX IX_EXN_SVC_CONFIG_ID (service_config_id);

--
-- Create table `voicemail_configuration`
--
CREATE TABLE voicemail_configuration (
  id int NOT NULL,
  backend_id varchar(50) NOT NULL,
  status bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 76,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_VOICEMAIL_CONFIGURATION_BACKEND_ID` on table `voicemail_configuration`
--
ALTER TABLE voicemail_configuration
ADD INDEX IX_VOICEMAIL_CONFIGURATION_BACKEND_ID (backend_id);

--
-- Create foreign key
--
ALTER TABLE voicemail_configuration
ADD CONSTRAINT FK_EXTERNAL_NUMBER_VOICEMAIL FOREIGN KEY (id)
REFERENCES external_number (id);

--
-- Create table `voicemail_subscription`
--
CREATE TABLE voicemail_subscription (
  id int NOT NULL AUTO_INCREMENT,
  subscription_id varchar(20) NOT NULL,
  voicemail_id int NOT NULL,
  endpoint varchar(300) NOT NULL,
  type varchar(10) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2339,
AVG_ROW_LENGTH = 200,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE voicemail_subscription
ADD CONSTRAINT FK_VOICEMAIL_SUBSCRIPTION_VOICEMAIL_CONFIGURATION FOREIGN KEY (voicemail_id)
REFERENCES voicemail_configuration (id);

--
-- Create table `inbound_capacity`
--
CREATE TABLE inbound_capacity (
  id int NOT NULL AUTO_INCREMENT,
  number_id int NOT NULL,
  capacity varchar(45) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 25583,
AVG_ROW_LENGTH = 67,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE inbound_capacity
ADD CONSTRAINT FK_INBOUND_CAPACITY_EXTERNAL_NUMBER FOREIGN KEY (number_id)
REFERENCES external_number (id);

--
-- Create table `external_number_whitelist`
--
CREATE TABLE external_number_whitelist (
  id int NOT NULL AUTO_INCREMENT,
  number_id int NOT NULL,
  user_number varchar(15) NOT NULL,
  start_date datetime NOT NULL,
  end_date datetime DEFAULT NULL,
  active bit(1) DEFAULT b'1',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 14421,
AVG_ROW_LENGTH = 150,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IXFK_EXTERNAL_NUMBER_WHITELIST_EXTERNAL_NUMBER` on table `external_number_whitelist`
--
ALTER TABLE external_number_whitelist
ADD INDEX IXFK_EXTERNAL_NUMBER_WHITELIST_EXTERNAL_NUMBER (number_id);

--
-- Create foreign key
--
ALTER TABLE external_number_whitelist
ADD CONSTRAINT FK_EXTERNAL_NUMBER_WHITELIST_EXTERNAL_NUMBER FOREIGN KEY (number_id)
REFERENCES external_number (id);