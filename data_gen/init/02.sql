CREATE DATABASE services_performance
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE services_performance;

--
-- Create table `tts_message`
--
CREATE TABLE tts_message (
  id int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 334256,
AVG_ROW_LENGTH = 27,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `tts_speak`
--
CREATE TABLE tts_speak (
  id int NOT NULL AUTO_INCREMENT,
  message_id int NOT NULL,
  speak_order int NOT NULL,
  voice int NOT NULL,
  language varchar(2) NOT NULL,
  gender varchar(50) NOT NULL,
  message longtext NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 405308,
AVG_ROW_LENGTH = 114,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_TTS_SPEAK_MSG_ID_ORDER` on table `tts_speak`
--
ALTER TABLE tts_speak
ADD INDEX IX_TTS_SPEAK_MSG_ID_ORDER (message_id, speak_order);

--
-- Create foreign key
--
ALTER TABLE tts_speak
ADD CONSTRAINT FK_TTS_SPEAK_TTS_MESSAGE FOREIGN KEY (message_id)
REFERENCES tts_message (id);

--
-- Create table `service_configuration`
--
CREATE TABLE service_configuration (
  id int NOT NULL AUTO_INCREMENT,
  service_uuid varchar(36) NOT NULL,
  waiting_msg_id int DEFAULT NULL,
  warning_msg_id int DEFAULT NULL,
  welcome_msg_id int DEFAULT NULL,
  incall_msg_id int DEFAULT NULL,
  outcall_msg_id int DEFAULT NULL,
  voicemail_msg_id int DEFAULT NULL,
  pincode_msg_id int DEFAULT NULL,
  pincode_sms_content longtext DEFAULT NULL,
  link_confirm_msg_id int DEFAULT NULL,
  click_to_call_confirm_msg_id int DEFAULT NULL,
  click_to_call_failure_msg_id int DEFAULT NULL,
  pickup_inbound_call bit(1) NOT NULL DEFAULT b'0',
  pickup_outbound_call bit(1) NOT NULL DEFAULT b'0',
  inbound_call_flow_type varchar(25) NOT NULL,
  active bit(1) NOT NULL DEFAULT b'1',
  country_code varchar(2) NOT NULL,
  inbound_presented_uri_type varchar(50) NOT NULL,
  external_numbers_allowed bit(1) NOT NULL DEFAULT b'0',
  inbound_ringing_timeout int NOT NULL,
  outbound_ringing_timeout int NOT NULL,
  inbound_pai_type varchar(20) NOT NULL,
  outbound_pai_type varchar(20) NOT NULL,
  pincode_validation_max_tries int NOT NULL,
  pincode_validation_max_delay int NOT NULL,
  add_diversion_header bit(1) NOT NULL DEFAULT b'0',
  max_contexts int NOT NULL DEFAULT 1,
  voicemail_group_uuid varchar(45) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 57763,
AVG_ROW_LENGTH = 222,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_SERVICE_UUID_ACTIVE` on table `service_configuration`
--
ALTER TABLE service_configuration
ADD INDEX IX_SERVICE_UUID_ACTIVE (service_uuid, active);

--
-- Create index `service_uuid` on table `service_configuration`
--
ALTER TABLE service_configuration
ADD UNIQUE INDEX service_uuid (service_uuid);

--
-- Create index `SERVICE_UUID_UNIQUE` on table `service_configuration`
--
ALTER TABLE service_configuration
ADD INDEX SERVICE_UUID_UNIQUE (service_uuid);

--
-- Create foreign key
--
ALTER TABLE service_configuration
ADD CONSTRAINT FK_SERVICE_CONFIGURATION_INCALL_MESSAGE FOREIGN KEY (incall_msg_id)
REFERENCES tts_message (id);

--
-- Create foreign key
--
ALTER TABLE service_configuration
ADD CONSTRAINT FK_SERVICE_CONFIGURATION_OUTCALL_MESSAGE FOREIGN KEY (outcall_msg_id)
REFERENCES tts_message (id);

--
-- Create foreign key
--
ALTER TABLE service_configuration
ADD CONSTRAINT FK_SERVICE_CONFIGURATION_PINCODE_MESSAGE FOREIGN KEY (pincode_msg_id)
REFERENCES tts_message (id);

--
-- Create foreign key
--
ALTER TABLE service_configuration
ADD CONSTRAINT FK_SERVICE_CONFIGURATION_VOICEMAIL_MESSAGE FOREIGN KEY (voicemail_msg_id)
REFERENCES tts_message (id);

--
-- Create foreign key
--
ALTER TABLE service_configuration
ADD CONSTRAINT FK_SERVICE_CONFIGURATION_WAITING_MESSAGE FOREIGN KEY (waiting_msg_id)
REFERENCES tts_message (id);

--
-- Create foreign key
--
ALTER TABLE service_configuration
ADD CONSTRAINT FK_SERVICE_CONFIGURATION_WARNING_MESSAGE FOREIGN KEY (warning_msg_id)
REFERENCES tts_message (id);

--
-- Create foreign key
--
ALTER TABLE service_configuration
ADD CONSTRAINT FK_SERVICE_CONFIGURATION_WELCOME_MESSAGE FOREIGN KEY (welcome_msg_id)
REFERENCES tts_message (id);

--
-- Create table `sms_configuration`
--
CREATE TABLE sms_configuration (
  id int NOT NULL,
  sms_enabled bit(1) NOT NULL DEFAULT b'1',
  sms_default_failure_message longtext DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 45,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE sms_configuration
ADD CONSTRAINT FK_SMS_CONFIGURATION_SERVICE_CONFIGURATION FOREIGN KEY (id)
REFERENCES service_configuration (id);

--
-- Create table `service_os_configuration`
--
CREATE TABLE service_os_configuration (
  id int NOT NULL AUTO_INCREMENT,
  service_config_id int NOT NULL,
  device_os varchar(45) NOT NULL,
  inbound_presented_uri_type varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1997,
AVG_ROW_LENGTH = 94,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE service_os_configuration
ADD CONSTRAINT FK_SERVICE_OS_CONFIGURATION_SERVICE_CONFIGURATION FOREIGN KEY (service_config_id)
REFERENCES service_configuration (id);

--
-- Create table `country`
--
CREATE TABLE country (
  country_code varchar(2) NOT NULL,
  PRIMARY KEY (country_code)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 68,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `service_outbound_country_whitelist`
--
CREATE TABLE service_outbound_country_whitelist (
  id int NOT NULL AUTO_INCREMENT,
  srv_config_id int NOT NULL,
  country_code varchar(2) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 87935,
AVG_ROW_LENGTH = 41,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE service_outbound_country_whitelist
ADD CONSTRAINT FK_SERVICE_OUTBOUND_COUNTRY_WHITELIST_COUNTRY FOREIGN KEY (country_code)
REFERENCES country (country_code);

--
-- Create foreign key
--
ALTER TABLE service_outbound_country_whitelist
ADD CONSTRAINT FK_SERVICE_OUTBOUND_COUNTRY_WHITELIST_SERVICE_CONFIGURATION FOREIGN KEY (srv_config_id)
REFERENCES service_configuration (id);

--
-- Create table `service_alias_country_whitelist`
--
CREATE TABLE service_alias_country_whitelist (
  id int NOT NULL AUTO_INCREMENT,
  srv_config_id int NOT NULL,
  virtual_number_country varchar(2) NOT NULL,
  user_number_country varchar(2) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 88280,
AVG_ROW_LENGTH = 41,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE service_alias_country_whitelist
ADD CONSTRAINT FK_SERVICE_ALIAS_COUNTRY_WHITELIST_COUNTRY FOREIGN KEY (virtual_number_country)
REFERENCES country (country_code);

--
-- Create foreign key
--
ALTER TABLE service_alias_country_whitelist
ADD CONSTRAINT FK_SERVICE_ALIAS_COUNTRY_WHITELIST_COUNTRY_02 FOREIGN KEY (user_number_country)
REFERENCES country (country_code);

--
-- Create foreign key
--
ALTER TABLE service_alias_country_whitelist
ADD CONSTRAINT FK_SERVICE_ALIAS_COUNTRY_WHITELIST_SERVICE_CONFIGURATION FOREIGN KEY (srv_config_id)
REFERENCES service_configuration (id);

--
-- Create table `service`
--
CREATE TABLE service (
  id int NOT NULL AUTO_INCREMENT,
  uuid varchar(36) NOT NULL,
  name varchar(50) NOT NULL,
  client_id varchar(36) DEFAULT NULL,
  active bit(1) NOT NULL DEFAULT b'1',
  retention_duration int NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 64866,
AVG_ROW_LENGTH = 164,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_CLIENT_UUID` on table `service`
--
ALTER TABLE service
ADD INDEX IX_CLIENT_UUID (client_id);

--
-- Create index `IX_SERVICE_UUID` on table `service`
--
ALTER TABLE service
ADD INDEX IX_SERVICE_UUID (uuid);