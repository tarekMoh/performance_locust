-- 
-- create the database
--
CREATE DATABASE sms_performance
CHARACTER SET utf8mb4
COLLATE utf8mb4_0900_ai_ci;


-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE sms_performance;

--
-- Create table `sms_configuration`
--
CREATE TABLE sms_configuration (
  id int NOT NULL AUTO_INCREMENT,
  incoming_comm_handling_id int NOT NULL,
  failure_email varchar(50) DEFAULT NULL,
  failure_http_base_path varchar(50) DEFAULT NULL,
  no_route_existed_msg varchar(250) DEFAULT NULL,
  route_closed_msg varchar(250) DEFAULT NULL,
  end_date datetime DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 598,
AVG_ROW_LENGTH = 137,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Create table `sms_notification_configuration`
--
CREATE TABLE sms_notification_configuration (
  id int NOT NULL AUTO_INCREMENT,
  sms_config_id int NOT NULL,
  channel_id int DEFAULT NULL,
  sub_id int DEFAULT NULL,
  notification_type varchar(10) NOT NULL,
  app_id varchar(10) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 318,
AVG_ROW_LENGTH = 51,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Create foreign key
--
ALTER TABLE sms_notification_configuration
ADD CONSTRAINT FK_SMS_NOTIFICATION_CONFIGURATION_SMS_CONFIGURATION FOREIGN KEY (sms_config_id)
REFERENCES sms_configuration (id);

--
-- Create table `sms_history`
--
CREATE TABLE sms_history (
  id int NOT NULL AUTO_INCREMENT,
  context_id varchar(50) DEFAULT NULL,
  service_config_id varchar(50) DEFAULT NULL,
  sender varchar(50) NOT NULL,
  recipient varchar(50) NOT NULL,
  type varchar(50) NOT NULL,
  content varchar(1000) DEFAULT NULL,
  start_date datetime NOT NULL,
  handling_date datetime DEFAULT NULL,
  sms_broker_sms_id varchar(50) DEFAULT NULL,
  outbound_status varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 208,
AVG_ROW_LENGTH = 267,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Create table `sms_handling_history`
--
CREATE TABLE sms_handling_history (
  id int NOT NULL AUTO_INCREMENT,
  sms_id int NOT NULL,
  behaviour_type varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 151,
AVG_ROW_LENGTH = 118,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Create foreign key
--
ALTER TABLE sms_handling_history
ADD CONSTRAINT FK_SMS_HANDLING_HISTORY FOREIGN KEY (sms_id)
REFERENCES sms_history (id);