-- 
-- create the database
--
CREATE DATABASE calls_performance
CHARACTER SET utf8mb4
COLLATE utf8mb4_0900_ai_ci;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE calls_performance;

--
-- Create table `incoming_comm_handling`
--
CREATE TABLE incoming_comm_handling (
  id int NOT NULL AUTO_INCREMENT,
  alias_number varchar(15) NOT NULL,
  mode varchar(50) NOT NULL,
  start_date datetime NOT NULL,
  end_date datetime DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4918,
AVG_ROW_LENGTH = 64,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_INCOMING_COMM_HANDLING_ALIAS_NUMBER` on table `incoming_comm_handling`
--
ALTER TABLE incoming_comm_handling
ADD INDEX IX_INCOMING_COMM_HANDLING_ALIAS_NUMBER (alias_number);

--
-- Create table `comm_behaviour`
--
CREATE TABLE comm_behaviour (
  id int NOT NULL AUTO_INCREMENT,
  comm_behaviour_type varchar(50) NOT NULL,
  redirection_number varchar(15) DEFAULT NULL,
  message_id int DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 10922,
AVG_ROW_LENGTH = 40,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `incoming_comm_handling_rule`
--
CREATE TABLE incoming_comm_handling_rule (
  id int NOT NULL AUTO_INCREMENT,
  handling_id int NOT NULL,
  rule_rank smallint NOT NULL DEFAULT 0,
  context_search_type varchar(50) DEFAULT NULL,
  context_found_bhv int NOT NULL,
  context_not_found_bhv int NOT NULL,
  last_contact_comparison_mode varchar(50) DEFAULT NULL,
  last_contact_comparison_value int DEFAULT NULL,
  last_contact_comparison_type varchar(50) DEFAULT NULL,
  last_contact_comparison_failure_bhv int DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7662,
AVG_ROW_LENGTH = 59,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE incoming_comm_handling_rule
ADD CONSTRAINT FK_INCOMING_CALL_HANDLING_RULE_CALL_BEHAVIOUR FOREIGN KEY (last_contact_comparison_failure_bhv)
REFERENCES comm_behaviour (id);

--
-- Create foreign key
--
ALTER TABLE incoming_comm_handling_rule
ADD CONSTRAINT FK_INCOMING_CALL_HANDLING_RULE_CALL_BEHAVIOUR_FOUND FOREIGN KEY (context_found_bhv)
REFERENCES comm_behaviour (id);

--
-- Create foreign key
--
ALTER TABLE incoming_comm_handling_rule
ADD CONSTRAINT FK_INCOMING_CALL_HANDLING_RULE_CALL_BEHAVIOUR_NOT_FOUND FOREIGN KEY (context_not_found_bhv)
REFERENCES comm_behaviour (id);

--
-- Create foreign key
--
ALTER TABLE incoming_comm_handling_rule
ADD CONSTRAINT FK_INCOMING_CALL_HANDLING_RULE_INCOMING_CALL_HANDLING FOREIGN KEY (handling_id)
REFERENCES incoming_comm_handling (id);

--
-- Create table `call_sessions`
--
CREATE TABLE call_sessions (
  id int NOT NULL AUTO_INCREMENT,
  context_uuid varchar(36) DEFAULT NULL,
  srv_config_uuid varchar(36) DEFAULT NULL,
  sess_uri varchar(255) NOT NULL,
  sess_caller varchar(255) NOT NULL,
  sess_initial_callee varchar(255) NOT NULL,
  sess_callee varchar(255) DEFAULT NULL,
  sess_second_uri varchar(255) DEFAULT NULL,
  start_date datetime NOT NULL,
  pickup_date datetime DEFAULT NULL,
  end_date datetime DEFAULT NULL,
  sess_call_privacy bit(1) NOT NULL DEFAULT b'0',
  sess_type varchar(50) NOT NULL,
  sess_presented_uri_prefix varchar(5) DEFAULT NULL,
  sess_presented_uri_type varchar(50) NOT NULL,
  sess_leg1_pani varchar(255) DEFAULT NULL,
  pai_type varchar(50) NOT NULL,
  dnd_mode varchar(50) NOT NULL,
  sess_read bit(1) NOT NULL DEFAULT b'0',
  sess_active bit(1) NOT NULL DEFAULT b'1',
  sess_leg1_to_uri varchar(255) NOT NULL,
  sess_leg1_pai varchar(255) DEFAULT NULL,
  sess_end_code int DEFAULT NULL,
  sess_version int NOT NULL DEFAULT 0,
  note longtext DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 10175021,
AVG_ROW_LENGTH = 425,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_CALL_SESSIONS_CONTEXT_UUID` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_CONTEXT_UUID (context_uuid);

--
-- Create index `IX_CALL_SESSIONS_DND_MODE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_DND_MODE (dnd_mode);

--
-- Create index `IX_CALL_SESSIONS_END_DATE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_END_DATE (end_date);

--
-- Create index `IX_CALL_SESSIONS_PAI_TYPE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_PAI_TYPE (pai_type);

--
-- Create index `IX_CALL_SESSIONS_SESS_CALLEE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_SESS_CALLEE (sess_callee);

--
-- Create index `IX_CALL_SESSIONS_SESS_CALLER` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_SESS_CALLER (sess_caller);

--
-- Create index `IX_CALL_SESSIONS_SESS_INIT_CALLEE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_SESS_INIT_CALLEE (sess_initial_callee);

--
-- Create index `IX_CALL_SESSIONS_SESS_PRESENTED_URI_TYPE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_SESS_PRESENTED_URI_TYPE (sess_presented_uri_type);

--
-- Create index `IX_CALL_SESSIONS_SESS_TYPE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_SESS_TYPE (sess_type);

--
-- Create index `IX_CALL_SESSIONS_SESS_URI` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_SESS_URI (sess_uri);

--
-- Create index `IX_CALL_SESSIONS_SRV_CONFIG_UUID` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_SRV_CONFIG_UUID (srv_config_uuid);

--
-- Create index `IX_CALL_SESSIONS_START_DATE` on table `call_sessions`
--
ALTER TABLE call_sessions
ADD INDEX IX_CALL_SESSIONS_START_DATE (start_date);

--
-- Create table `uncertain_call`
--
CREATE TABLE uncertain_call (
  call_id int NOT NULL AUTO_INCREMENT,
  sess_id int NOT NULL,
  PRIMARY KEY (call_id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE uncertain_call
ADD CONSTRAINT FK_IKEA_UNCERTAIN_CALL_IKEA_CALL_SESSION FOREIGN KEY (sess_id)
REFERENCES call_sessions (id);

--
-- Create table `call_handling_history`
--
CREATE TABLE call_handling_history (
  id int NOT NULL AUTO_INCREMENT,
  call_session_id int NOT NULL,
  behaviour_type varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 393655155,
AVG_ROW_LENGTH = 86,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE call_handling_history
ADD CONSTRAINT FK_CALL_HANDLING_HISTORY_OSP_CALL_SESSION FOREIGN KEY (call_session_id)
REFERENCES call_sessions (id);

--
-- Create table `incoming_comm_route`
--
CREATE TABLE incoming_comm_route (
  id int NOT NULL AUTO_INCREMENT,
  context_uuid varchar(36) NOT NULL,
  caller_number varchar(15) NOT NULL,
  start_date datetime NOT NULL,
  end_date datetime DEFAULT NULL,
  note longtext DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6641436,
AVG_ROW_LENGTH = 96,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_INCOMING_CALL_ROUTE_USAGE_CONTEXT` on table `incoming_comm_route`
--
ALTER TABLE incoming_comm_route
ADD INDEX IX_INCOMING_CALL_ROUTE_USAGE_CONTEXT (context_uuid);

--
-- Create table `imminent_call`
--
CREATE TABLE imminent_call (
  id int NOT NULL AUTO_INCREMENT,
  context_uuid varchar(36) NOT NULL,
  callee_number varchar(50) NOT NULL,
  creation_date datetime NOT NULL,
  session_type varchar(50) NOT NULL,
  context_user_number varchar(15) DEFAULT NULL,
  context_user_sip_uri varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1869,
AVG_ROW_LENGTH = 148,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_IMMINENT_CALL_CONTEXT_UUID` on table `imminent_call`
--
ALTER TABLE imminent_call
ADD INDEX IX_IMMINENT_CALL_CONTEXT_UUID (context_uuid);