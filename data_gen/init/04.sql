CREATE DATABASE contexts_performance
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

--
-- Set default database
--
USE contexts_performance;

--
-- Create table `voicemail_configuration`
--
CREATE TABLE voicemail_configuration (
  id int NOT NULL AUTO_INCREMENT,
  backend_id varchar(50) NOT NULL,
  status bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 10507,
AVG_ROW_LENGTH = 64,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_VOICEMAIL_CONFIGURATION_BACKEND_ID` on table `voicemail_configuration`
--
ALTER TABLE voicemail_configuration
ADD INDEX IX_VOICEMAIL_CONFIGURATION_BACKEND_ID (backend_id);

--
-- Create table `voicemail_subscription`
--
CREATE TABLE voicemail_subscription (
  id int NOT NULL AUTO_INCREMENT,
  subscription_id varchar(20) NOT NULL,
  voicemail_id int NOT NULL,
  endpoint varchar(300) NOT NULL,
  type varchar(10) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 18851,
AVG_ROW_LENGTH = 214,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE voicemail_subscription
ADD CONSTRAINT FK_VOICEMAIL_SUBSCRIPTION_VOICEMAIL_CONFIGURATION FOREIGN KEY (voicemail_id)
REFERENCES voicemail_configuration (id);

--
-- Create table `do_not_disturb`
--
CREATE TABLE do_not_disturb (
  id int NOT NULL AUTO_INCREMENT,
  active bit(1) NOT NULL DEFAULT b'1',
  full_mode bit(1) NOT NULL DEFAULT b'0',
  full_mode_number varchar(15) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2390,
AVG_ROW_LENGTH = 80,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `usage_context`
--
CREATE TABLE usage_context (
  id int NOT NULL AUTO_INCREMENT,
  uuid varchar(36) NOT NULL,
  service_config_uuid varchar(36) NOT NULL,
  user_number varchar(15) DEFAULT NULL,
  user_sip_uri varchar(50) DEFAULT NULL,
  alias_number varchar(15) NOT NULL,
  pincode_status varchar(50) NOT NULL,
  pincode varchar(4) DEFAULT NULL,
  pincode_activation_tries int DEFAULT NULL,
  pincode_expiration_date datetime DEFAULT NULL,
  pincode_activation_number_type varchar(50) DEFAULT NULL,
  creation_date datetime NOT NULL,
  expiration_date datetime NOT NULL,
  dnd_id int DEFAULT NULL,
  voicemail_id int DEFAULT NULL,
  alias_number_type varchar(20) NOT NULL,
  note longtext DEFAULT NULL,
  preferred bit(1) NOT NULL DEFAULT b'1',
  admin bit(1) NOT NULL DEFAULT b'0',
  sms_notification_email varchar(254) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 74570,
AVG_ROW_LENGTH = 354,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `IX_CTX_ALIAS_NUMBER` on table `usage_context`
--
ALTER TABLE usage_context
ADD INDEX IX_CTX_ALIAS_NUMBER (alias_number);

--
-- Create index `IX_CTX_EXPIRATION_DATE` on table `usage_context`
--
ALTER TABLE usage_context
ADD INDEX IX_CTX_EXPIRATION_DATE (expiration_date);

--
-- Create index `IX_CTX_PREFERRED` on table `usage_context`
--
ALTER TABLE usage_context
ADD INDEX IX_CTX_PREFERRED (preferred);

--
-- Create index `IX_CTX_SERVICE_CONFIG_UUID` on table `usage_context`
--
ALTER TABLE usage_context
ADD INDEX IX_CTX_SERVICE_CONFIG_UUID (service_config_uuid);

--
-- Create index `IX_CTX_USER_NUMBER` on table `usage_context`
--
ALTER TABLE usage_context
ADD INDEX IX_CTX_USER_NUMBER (user_number);

--
-- Create index `UQ_USAGE_CONTEXT_CTX_UUID` on table `usage_context`
--
ALTER TABLE usage_context
ADD INDEX UQ_USAGE_CONTEXT_CTX_UUID (uuid);

--
-- Create foreign key
--
ALTER TABLE usage_context
ADD CONSTRAINT FK_USAGE_CONTEXT_DO_NOT_DISTURB FOREIGN KEY (dnd_id)
REFERENCES do_not_disturb (id);

--
-- Create foreign key
--
ALTER TABLE usage_context
ADD CONSTRAINT FK_USAGE_CONTEXT_VOICEMAIL FOREIGN KEY (voicemail_id)
REFERENCES voicemail_configuration (id);

--
-- Create table `sms_notification_configuration`
--
CREATE TABLE sms_notification_configuration (
  id int NOT NULL AUTO_INCREMENT,
  context_id int NOT NULL,
  channel_id int DEFAULT NULL,
  sub_id int DEFAULT NULL,
  notification_type varchar(10) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 552,
AVG_ROW_LENGTH = 98,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Create foreign key
--
ALTER TABLE sms_notification_configuration
ADD CONSTRAINT FK_SMS_NOTIFICATION_CONFIGURATION_USAGE_CONTEXT FOREIGN KEY (context_id)
REFERENCES usage_context (id);

--
-- Create table `notification_configuration`
--
CREATE TABLE notification_configuration (
  id int NOT NULL AUTO_INCREMENT,
  context_id int NOT NULL,
  channel_id int DEFAULT NULL,
  sub_id int DEFAULT NULL,
  application_id varchar(255) DEFAULT NULL,
  active bit(1) NOT NULL DEFAULT b'1',
  platform varchar(10) NOT NULL,
  device_os varchar(45) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 510,
AVG_ROW_LENGTH = 157,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE notification_configuration
ADD CONSTRAINT FK_NOTIFICATION_CONFIGURATION_USAGE_CONTEXT FOREIGN KEY (context_id)
REFERENCES usage_context (id);

--
-- Create table `context_capability`
--
CREATE TABLE context_capability (
  id int NOT NULL AUTO_INCREMENT,
  context_id int NOT NULL,
  capability_type varchar(50) NOT NULL,
  value varchar(50) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 54079,
AVG_ROW_LENGTH = 60,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE context_capability
ADD CONSTRAINT FK_CONTEXT_CAPABILITY_USAGE_CONTEXT FOREIGN KEY (context_id)
REFERENCES usage_context (id);

--
-- Create table `calender_event`
--
CREATE TABLE calender_event (
  id int NOT NULL AUTO_INCREMENT,
  first_start_date datetime DEFAULT NULL,
  first_end_date datetime DEFAULT NULL,
  rrule varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5432,
AVG_ROW_LENGTH = 97,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `dnd_event`
--
CREATE TABLE dnd_event (
  id int NOT NULL AUTO_INCREMENT,
  dnd_id int NOT NULL,
  calender int NOT NULL,
  message_id int NOT NULL,
  number varchar(15) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5432,
AVG_ROW_LENGTH = 97,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE dnd_event
ADD CONSTRAINT FK_CALENDER_ID FOREIGN KEY (calender)
REFERENCES calender_event (id);

--
-- Create foreign key
--
ALTER TABLE dnd_event
ADD CONSTRAINT FK_DONOTDISTURB_ID FOREIGN KEY (dnd_id)
REFERENCES do_not_disturb (id);