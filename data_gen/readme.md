## Managing test data in end-to-end test automation
- For unit and integration testing, it is often a good idea to resort to mocking or stubbing the data layer to remain in control over the test data that is used in and required for the tests to be executed. 

- for e2e  

- possible strategies for dealing with test data in end-to-end tests

### how to run
- to run locally go to data folder and run 'python scripts/main.py'
- run from docker compose 'docker-compose up -d'