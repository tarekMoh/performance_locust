import logging
import uuid
import random
import json
import csv, sys
import pandas as pd
from random import randint
import string
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from ast import literal_eval
from sshtunnel import SSHTunnelForwarder
import paramiko

def fake_french_numbers(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return "+336"+str(randint(range_start, range_end))

def fake_number(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return "+336"+str(randint(range_start, range_end))

def random_char(y):
       return ''.join(random.choice(string.ascii_letters) for x in range(y))

def extract_data(DBParams,filename):
    engine = create_engine(DBParams, echo=False)
    outfile = open(filename+'.csv', 'wb')
    outcsv = csv.writer(outfile)
    cursor = engine.execute('select * from client')
    print(cursor)
    # dump column titles (optional)
    #outcsv.writerow(x[0] for x in cursor.row)
    # dump rows
    outcsv.writerows(cursor.fetchall())
    outfile.close()

def progressBar(iterable, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    total = len(iterable)
    # Progress Bar Printing Function
    def printProgressBar (iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Initial Call
    printProgressBar(0)
    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()