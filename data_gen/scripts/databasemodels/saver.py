import csv

from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = create_engine('mysql://root:root@127.0.0.1:3307', echo=False)
Session = sessionmaker(bind=engine)


def get_routes():
    result = engine.execute(
        ''' SELECT
                apps.app_id AS appid,
                srv.uuid AS serviceid,
                '2021-01-01' AS 'startdate',
                '3021-01-01' AS 'enddate',
                ELT(0.5 + RAND() * 2, 'true', 'false') AS status,
                REPLACE(JSON_EXTRACT(uctxt.note, "$.aliasName"),'"','') AS username,
                '0' AS 'offset',
                '100' AS 'limit'  
            FROM contexts_performance.usage_context uctxt,
                 services_performance.service srv,
                 clients_performance.client clints,
                 clients_performance.application apps
            WHERE uctxt.service_config_uuid = srv.uuid
                 AND clints.uuid = srv.client_id
                 AND apps.client_id = clints.id;''')

    outfile = open('get_routes.csv', 'w', newline='')
    outcsv = csv.writer(outfile, delimiter=',')
    outcsv.writerow(result.keys())
    outcsv.writerows(result.fetchall())


def get_routes():
    result = engine.execute(
        ''' SELECT
                apps.app_id AS appid,
                srv.uuid AS serviceid,
                uctxt.user_number AS usernumber,
                REPLACE(uctxt.alias_number, '+','')  AS aliasnumber,
                LEFT(MD5(RAND()), 1000) AS note
                FROM contexts_performance.usage_context uctxt,
                    services_performance.service srv,
                    clients_performance.client clints,
                    clients_performance.application apps
                WHERE uctxt.service_config_uuid = srv.uuid
                AND clints.uuid = srv.client_id
                AND uctxt.pincode_status = "ACTIVATED"
                AND apps.client_id = clints.id;''')
    outfile = open('create_routes.csv', 'w', newline='')
    outcsv = csv.writer(outfile, delimiter=',')
    outcsv.writerow(result.keys())
    outcsv.writerows(result.fetchall())
get_routes()
create_routes()    