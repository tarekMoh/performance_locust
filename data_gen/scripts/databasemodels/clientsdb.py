import logging, uuid, random
import pandas as pd
import itertools    
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from ast import literal_eval
from .helper import *
from time import sleep

fake = Faker('fr_FR')
CLIENTS_DBParams = 'mysql://root:root@127.0.0.1:3307/clients_performance'
engine = create_engine(CLIENTS_DBParams, echo=False)
insp = reflection.Inspector.from_engine(engine)

class DatabaseClients(): 
    def __init__(self,NO_CLIENTS,NO_APPS):
        self.NO_CLIENTS=NO_CLIENTS
        self.NO_APPS=NO_APPS
        self.clean_db()
        print("Generating client data")
        self.clients()
        print("Generating application data")
        self.applications()

    def clean_db(self):
        with engine.connect() as con:
            data = insp.get_table_names()
            for line in data:
                print("""DELETE FROM clients_performance."""+line)
                con.execute("""DELETE FROM clients_performance."""+line)

    def clients(self):
        clients = pd.DataFrame(columns = ['uuid', 'name','numbers_search_allowed'])
        clients = pd.DataFrame(index=range(1,self.NO_CLIENTS+1))
        clients.index.name='id'
        clients['uuid'] = clients.index.map(lambda x : fake.unique.uuid4())
        clients['name'] = clients.index.map(lambda x : fake.unique.name())
        clients['numbers_search_allowed'] = clients.index.map(lambda x : fake.boolean(chance_of_getting_true=100))
        print(clients.head(10))
        #clients.to_csv("scripts/csv/dump/clients.csv", sep=',', encoding='utf-8')
        clients.to_sql("client", con=engine, if_exists="append", index=False)
        table_df = pd.read_sql_table('client',con=engine)
        table_df.to_csv("scripts/csv/dump/clients.csv", sep=',', encoding='utf-8', index=False)

    def applications(self):
        applications = pd.DataFrame(columns = ['app_id', 'app_name','client_id'])
        apps_df = pd.read_csv("scripts/csv/dump/clients.csv", usecols=[0])
        for row in apps_df.itertuples():
            app_id = ''.join(random.choice('0123456789ABCDEF') for i in range(10))
            app_name = fake.unique.company()
            client_id = row.id
            applications = applications.append({'app_id':app_id,'app_name':app_name,'client_id':client_id},ignore_index=True)
            print(applications)
        # save data to csv
        applications.to_csv("scripts/csv/dump/applications.csv", sep=',', encoding='utf-8', index=False)
        applications.to_sql("application", con=engine, if_exists="append", chunksize=100, index=False)
