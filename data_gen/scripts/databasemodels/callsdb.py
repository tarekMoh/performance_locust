import logging, uuid, random, datetime
import pandas as pd
from random import randint    
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from ast import literal_eval
from datetime import datetime, timedelta

engine = create_engine('mysql://root:root@127.0.0.1:3307/calls_performance', echo=False)
# instantiate the database inspector of sqlalchemy to get table names, etc...
insp = reflection.Inspector.from_engine(engine)
fake = Faker('fr_FR')

def fake_french_numbers(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return "+336"+str(randint(range_start, range_end))

class DatabaseCalls(): 
    def __init__(self,NO_Routes, NO_CALLS):
        self.NO_Routes=NO_Routes
        self.NO_CALLS=NO_CALLS
        # print("database tables: ")
        # print(", ".join(map(str, insp.get_table_names())))
        # self.usage_context()
        self.clean_db()
        print("Generating calls db data")
        self.incoming_comm_route()
        print("Generating call session data")
        self.call_sessions()

    def clean_db(self):
        with engine.connect() as con:
            data = insp.get_table_names()
            for line in data:
                print("""DELETE FROM calls_performance."""+line)
                con.execute("""DELETE FROM calls_performance."""+line)

    def incoming_comm_route(self):
        incoming_comm_route = pd.DataFrame(columns = ['context_uuid', 'caller_number','start_date','end_date','note'])
        apps_df = pd.read_csv("scripts/csv/dump/usage_context.csv", usecols=[1])
        for row in apps_df.itertuples():
            context_id    = row.uuid
            caller_number = fake_french_numbers(8)
            start_date    = datetime.now()
            end_date      = None
            note          = fake.paragraph()
            print(incoming_comm_route)
            incoming_comm_route = incoming_comm_route.append({'context_uuid': context_id, 'caller_number':caller_number, 'start_date':start_date, 'end_date':end_date, 'note':note },ignore_index=True)
            
        # save data to csv
        incoming_comm_route.to_csv("scripts/csv/dump/incoming_comm_route.csv", sep=',', encoding='utf-8', index=False)
        incoming_comm_route.to_sql("incoming_comm_route", con=engine, if_exists="append", chunksize=100, index=False)        



    def call_sessions(self):
        call_sessions = pd.DataFrame(columns = ['context_uuid', 'srv_config_uuid','sess_uri',
                                                'sess_caller','sess_initial_callee',
                                                'sess_callee','sess_second_uri','start_date',
                                                'pickup_date','end_date', 'sess_call_privacy', 'sess_type',
                                                'sess_presented_uri_prefix','sess_presented_uri_type',
                                                'sess_leg1_pani','pai_type','dnd_mode','sess_read','sess_active',
                                                'sess_leg1_to_uri','sess_leg1_pai','sess_end_code','sess_version','note'])

        apps_df = pd.read_csv("scripts/csv/dump/usage_context.csv", usecols=[1, 2, 3,5])
        print(apps_df.head())
        for row in apps_df.itertuples():
            for x in range(self.NO_CALLS):
                context_uuid              = row.uuid
                srv_config_uuid           = row.service_config_uuid
                sess_uri                  = "sessionid:sessionIdPart_0-36c1:tel%3A%2B"+str(row.alias_number)
                sess_caller               = "tel:+33634377101"
                sess_initial_callee       = "tel:+33684393142"
                sess_callee               = "tel:+33684393142"
                sess_second_uri           = "sessionid:sessionIdPart_7-e90e:tel%3A%2B33698217644"
                start_date                = (datetime.utcnow()+timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")
                pickup_date               = (datetime.utcnow()+timedelta(seconds=20)).strftime("%Y-%m-%d %H:%M:%S")
                end_date                  = (datetime.utcnow()+timedelta(seconds=360)).strftime("%Y-%m-%d %H:%M:%S")
                sess_call_privacy         = fake.boolean(chance_of_getting_true=50) 
                sess_type                 = str(random.choice(['inbound', 'outbound', 'clickToCall']))
                sess_presented_uri_prefix = None
                sess_presented_uri_type   = str(random.choice(['CALLER', 'PLATFORM']))
                sess_leg1_pani            = "GSTN; operator-specific-GI=""610000000"";network-provided"
                pai_type                  = str(random.choice(['CALLER', 'PLATFORM']))
                dnd_mode                  = fake.boolean(chance_of_getting_true=50) 
                sess_read                 = fake.boolean(chance_of_getting_true=50) 
                sess_active               = fake.boolean(chance_of_getting_true=100)
                sess_leg1_to_uri          = "sip:+33969744100@pfs-aliasnumber.fr;user=phone"
                sess_leg1_pai             = "tel:+33634377101"
                sess_end_code             = str(random.choice(['200','404','480','500']))
                sess_version              = str(random.choice(['0','1','2','3','4','5','6','7','8','9']))
                note                      = fake.paragraph()
                print(call_sessions)
                call_sessions = call_sessions.append({'context_uuid': context_uuid, 
                                'srv_config_uuid':srv_config_uuid, 'sess_uri':sess_uri,"sess_caller":sess_caller ,'sess_initial_callee':sess_initial_callee, 
                                'sess_callee':sess_callee, 'sess_second_uri':sess_second_uri, 'start_date':start_date, 
                                'pickup_date':pickup_date, 'end_date':end_date, 'sess_call_privacy':sess_call_privacy,
                                'sess_type':sess_type, 'sess_presented_uri_prefix':sess_presented_uri_prefix, 'sess_presented_uri_type':sess_presented_uri_type, 
                                'sess_leg1_pani':sess_leg1_pani, 'pai_type':pai_type, 'dnd_mode':dnd_mode, 'sess_read':sess_read, 
                                'sess_active':sess_active, 'sess_leg1_to_uri':sess_leg1_to_uri, 'sess_leg1_pai':sess_leg1_pai, 
                                'sess_end_code':sess_end_code, 'sess_version':sess_version, 'note':note },ignore_index=True)            
        # save data to csv
        call_sessions.to_csv("scripts/csv/dump/call_sessions.csv", sep=',', encoding='utf-8', index=False)
        call_sessions.to_sql("call_sessions", con=engine, if_exists="append", chunksize=100, index=False)        