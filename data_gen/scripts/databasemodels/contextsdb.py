import logging, uuid, random, json, datetime
import pandas as pd
from random import randint   
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from ast import literal_eval
from .helper import *

fake = Faker('fr_FR')
CONTEXTS_DBParams = 'mysql://root:root@localhost:3307/contexts_performance'
engine = create_engine(CONTEXTS_DBParams, echo=False)
insp = reflection.Inspector.from_engine(engine)

class DatabaseContexts(): 
    def __init__(self,NO_CONTEXTS):
        self.NO_CONTEXTS=NO_CONTEXTS
        self.clean_db()
        print("Generating context db data")
        self.usage_context()
        self.context_capability()

    def clean_db(self):
        with engine.connect() as con:
            data = insp.get_table_names()
            for line in data:
                print("""DELETE FROM contexts_performance."""+line)
                con.execute("""DELETE FROM contexts_performance."""+line)

    def usage_context(self):
        aliasnumber_df = pd.read_csv("scripts/csv/dump/numbers.csv").rename(columns={'id': 'number_id'})
        usernumber_df = pd.read_csv("scripts/csv/dump/external_number_whitelist.csv")
        merged = aliasnumber_df.merge(usernumber_df, on='number_id')
        merged.to_csv("scripts/csv/dump/temp.csv", index=False)
        numberdata = pd.read_csv("scripts/csv/dump/temp.csv")
        #apps_df = pd.read_csv("scripts/csv/dump/service.csv", usecols=[1])
        usage_context = pd.DataFrame(columns = ['uuid', 'service_config_uuid', 'user_number', 'user_sip_uri', 'alias_number', 'pincode_status', 'pincode', 'pincode_activation_tries', 'pincode_expiration_date', 'pincode_activation_number_type', 'creation_date', 'expiration_date', 'dnd_id', 'voicemail_id', 'alias_number_type', 'note', 'preferred', 'admin','sms_notification_email'])        
        for row in numberdata.itertuples():
            a = fake.first_name()
            b = fake.last_name()
            uuid = fake.uuid4()
            service_config_uuid = row.service_config_id
            user_number = "+"+str(row.user_number)
            user_sip_uri = ''
            alias_number = "+"+str(row.number)
            pincode_status = 'ACTIVATED'
            pincode  = ''
            pincode_activation_tries = 3
            pincode_expiration_date = None
            pincode_activation_number_type = ''
            creation_date= datetime.datetime.now()
            expiration_date= datetime.datetime(2121, 5, 10, 6, 51, 36, 462962)
            dnd_id = None
            voicemail_id = None
            alias_number_type = 'EXTERNAL'
            note = json.dumps({"firstname": a, "lastname": b, "login": a + "." + b + "@orange.com", "aliasName": a, 'username':a})
            preferred = True
            admin = False
            sms_notification_email= 'test@orange.com'
            usage_context = usage_context.append({'uuid': uuid, 'service_config_uuid':service_config_uuid, 'user_number':user_number, 'user_sip_uri':user_sip_uri, 'alias_number':alias_number, 'pincode_status':pincode_status,'pincode':pincode, 'pincode_activation_tries':pincode_activation_tries, 'pincode_expiration_date':pincode_expiration_date, 'pincode_activation_number_type':pincode_activation_number_type, 'creation_date':creation_date, 'expiration_date':expiration_date, 'dnd_id':dnd_id, 'voicemail_id':voicemail_id, 'alias_number_type':alias_number_type, 'note':note, 'preferred':preferred, 'admin':admin, 'sms_notification_email':sms_notification_email },ignore_index=True)
        #usage_context['id'] = usage_context.index
        #usage_context['alias_number'] = aliasnumber_df[['number']]
        #usage_context['user_number'] = usernumber_df[['user_number']]
            print(usage_context)
        #print(applications.head())
        usage_context.to_sql("usage_context", con=engine, if_exists="append", chunksize=100, index=False)        
        #usage_context.to_csv("scripts/csv/dump/usage_context.csv", sep=',', encoding='utf-8',index_label='id') # , index=True, '

        # save data to csv
        table_df = pd.read_sql_table('usage_context',con=engine)
        table_df.to_csv("scripts/csv/dump/usage_context.csv", sep=',', encoding='utf-8', index=False)



    def context_capability(self):
        context_capability = pd.DataFrame(columns = ['context_id', 'capability_type', 'value' ])
        apps_df = pd.read_csv("scripts/csv/dump/usage_context.csv", usecols=[0])
        # for row in apps_df.itertuples():
        #     context_id = row.id
        #     capability_type = str(random.choice(['CALL_IN']))
        #     value = True
        #     context_capability = context_capability.append({'context_id': context_id, 'capability_type':capability_type, 'value':value},ignore_index=True)
        #     print(context_capability)
        for row in apps_df.itertuples():
            context_id = row.id
            capability_type = str(random.choice(['CALL_IN','CALL_OUT','SMS_MT','SMS_MO']))
            value = True
            context_capability = context_capability.append({'context_id': context_id, 'capability_type':capability_type, 'value':value},ignore_index=True)
            print(context_capability)
        # save data to csv
        context_capability.to_csv("scripts/csv/dump/context_capability.csv", sep=',', encoding='utf-8', index=False)
        context_capability.to_sql("context_capability", con=engine, if_exists="append", chunksize=100, index=False)        
    # def calender_event(self):
    # def dnd_event(self):
    # def do_not_disturb(self):
    # def notification_configuration(self):
    # def voicemail_configuration(self):
    # def voicemail_subscription(self):