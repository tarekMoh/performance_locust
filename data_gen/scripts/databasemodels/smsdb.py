import logging, uuid, random, datetime
import pandas as pd
from random import randint    
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from ast import literal_eval
from datetime import datetime, timedelta
from .helper import *

engine = create_engine('mysql://root:root@127.0.0.1:3307/sms_performance', echo=False)
# instantiate the database inspector of sqlalchemy to get table names, etc...
# engine = create_engine(SMS_DBParams, echo=False)
insp = reflection.Inspector.from_engine(engine)
fake = Faker('fr_FR')

# def fake_french_numbers(n):
#     range_start = 10**(n-1)
#     range_end = (10**n)-1
#     return "+336"+str(randint(range_start, range_end))

class DatabaseSMS(): 
    def __init__(self,NO_SMS):
        self.NO_SMS=NO_SMS
        self.clean_db()
        print("Generating sms history db data")
        self.sms_history()
        print("Generating sms handling history db data")
        self.sms_handling_history()
        print("Generating sms configuration db data")
        self.sms_configuration()
        print("Generating sms notification configuration db data")
        self.sms_notification_configuration()

    def clean_db(self):
        with engine.connect() as con:
            data = insp.get_table_names()
            for line in data:
                print("""DELETE FROM sms_performance."""+line)
                con.execute("""DELETE FROM sms_performance."""+line)

    def sms_history(self):
        sms_history = pd.DataFrame(columns = ['context_id', 'service_config_id','sender','recipient','type','content','start_date','handling_date','sms_broker_sms_id','outbound_status'])
        apps_df = pd.read_csv("scripts/csv/dump/usage_context.csv", usecols=[1])
        for row in apps_df.itertuples():
            context_id    = row.uuid
            service_config_id = fake.uuid4()
            sender = fake_french_numbers(8)
            recipient = fake_french_numbers(8)
            type = 'SMS_MO'
            content = str(random.choice(['test msg', 'sms test', 'nominal case test', 'test test test test test test test test test']))
            start_date    = datetime.now()
            handling_date   = datetime.now()
            sms_broker_sms_id   = fake.uuid4()
            outbound_status = None
            print(sms_history)
            sms_history = sms_history.append({'context_id': context_id, 'service_config_id':service_config_id, 'sender':sender, 'recipient':recipient, 'type':type, 'content':content, 'start_date':start_date, 'handling_date':handling_date, 'sms_broker_sms_id':sms_broker_sms_id, 'outbound_status':outbound_status },ignore_index=True)
            
        # save data to csv
        sms_history.to_csv("scripts/csv/dump/sms_history.csv", sep=',', encoding='utf-8', index=False)
        sms_history.to_sql("sms_history", con=engine, if_exists="append", chunksize=100, index=False)        

    def sms_handling_history(self):
        sms_handling_history = pd.DataFrame(columns = ['sms_uuid', 'behaviour_type'])
        apps_df = pd.read_csv("scripts/csv/dump/sms_history.csv", usecols=[0])
        for row in apps_df.itertuples():
            sms_id    = row.id
            behaviour_type = str(random.choice(['NO_ENDING_RULE', 'ROUTE_CLOSED', 'TRANSMITTED', 'NO_ROUTE_EVER_EXISTED', 'NO_INBOUND_RULE_SET']))
            print(sms_handling_history)
            sms_handling_history = sms_handling_history.append({'sms_uuid': sms_id, 'behaviour_type':behaviour_type },ignore_index=True)
            
        # save data to csv
        sms_handling_history.to_csv("scripts/csv/dump/sms_handling_history.csv", sep=',', encoding='utf-8', index=False)
        sms_handling_history.to_sql("sms_handling_history", con=engine, if_exists="append", chunksize=100, index=False)

    def sms_configuration(self):
        sms_configuration = pd.DataFrame(columns = ['incoming_comm_handling_id', 'failure_email','failure_http_base_path',
                                                    'no_route_existed_msg','route_closed_msg','end_date'])

        # apps_df = pd.read_csv("scripts/csv/dump/usage_context.csv", usecols=[1, 2, 3,5])
        sms_configuration = pd.DataFrame(index=range(1,self.NO_CLIENTS+1))
        sms_configuration.index.name='id'
        print(sms_configuration.head())
        for x in range(self.NO_SMS):
            incoming_comm_handling_id = fake_number()
            failure_email             = str(random.choice([random_char(7)+"@gmail.com", None]))
            failure_http_base_path    = str(random.choice(['https://www.client.com/failure', 'null']))
            no_route_existed_msg      = str(random.choice(['There is no Route Ever Existed.', None, 'Bonjour, nous ne pouvons pas traiter votre message']))
            route_closed_msg          = str(random.choice(['The route has been closed.', 'null', 'Bonjour, votre dossier a ete traite. Merci de contacter notre service client']))
            end_date                  = (datetime.utcnow()+timedelta(seconds=360)).strftime("%Y-%m-%d %H:%M:%S")
            print(sms_configuration)
            sms_configuration = sms_configuration.append({'incoming_comm_handling_id': incoming_comm_handling_id, 
                            'failure_email':failure_email, 'failure_http_base_path':failure_http_base_path,"no_route_existed_msg":no_route_existed_msg, 
                            'route_closed_msg':route_closed_msg, 'end_date':end_date},ignore_index=True)            
        # save data to csv
        sms_configuration.to_csv("scripts/csv/dump/sms_configuration.csv", sep=',', encoding='utf-8', index=False)
        sms_configuration.to_sql("sms_configuration", con=engine, if_exists="append", chunksize=100, index=False)   

    def sms_notification_configuration(self):
        sms_notification_configuration = pd.DataFrame(columns = ['sms_config_id', 'channel_id', 'sub_id', 'notification_type', 'app_id'])
        apps_df = pd.read_csv("scripts/csv/dump/sms_configuration.csv", usecols=[1])
        for row in apps_df.itertuples():
            sms_config_id = row.uuid
            channel_id    = fake_number(5)
            sub_id        = fake_number(6)
            notification_type = str(random.choice(['HTTP', 'EMAIL', 'TRANSMITTED', 'NO_ROUTE_EVER_EXISTED', 'NO_INBOUND_RULE_SET']))
            app_id = 'SMS'
            print(sms_notification_configuration)
            sms_notification_configuration = sms_notification_configuration.append({'sms_config_id': sms_config_id, 'channel_id':channel_id, 'sub_id':sub_id, 'notification_type':notification_type, 'app_id':app_id },ignore_index=True)
            
        # save data to csv
        sms_notification_configuration.to_csv("scripts/csv/dump/sms_notification_configuration.csv", sep=',', encoding='utf-8', index=False)
        sms_notification_configuration.to_sql("sms_notification_configuration", con=engine, if_exists="append", chunksize=100, index=False)         