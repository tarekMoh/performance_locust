import logging, uuid, random
import pandas as pd
import itertools    
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from ast import literal_eval
from .helper import *

fake = Faker('fr_FR')
SERVICES_DBParams = 'mysql://root:root@localhost:3307/services_performance'
engine = create_engine(SERVICES_DBParams, echo=False)
insp = reflection.Inspector.from_engine(engine)

class DatabaseServices(): 
    def __init__(self,NO_SERVICES):
        self.NO_SERVICES=NO_SERVICES
        self.clean_db()
        print("Generating service db data")
        self.country()
        self.services()
        self.tts_message()
        self.service_configuration()
        self.service_alias_country_whitelist()
        self.sms_configuration()    
        self.service_os_configuration()           
        self.tts_speak()
        self.service_outbound_country_whitelist()

    def clean_db(self):
        with engine.connect() as con:
            data = ['service',  
                    'service_alias_country_whitelist','sms_configuration','service_outbound_country_whitelist',
                    'service_os_configuration', 'country','service_configuration', 
                    'tts_speak','tts_message']
            for line in data:
                print("""DELETE FROM services_performance."""+line)
                con.execute("""DELETE FROM services_performance."""+line)

    def services(self):
        services = pd.DataFrame(columns = ['uuid','name','client_id','active','retention_duration'])
        apps_df = pd.read_csv("scripts/csv/dump/clients.csv", usecols=[1])
        for row in apps_df.itertuples():
            uuid = fake.uuid4()
            name = fake.unique.bothify(text='Service_????-########', letters='ABCDEFGHIJKLMNOPQRSTUVWXYZ')
            client_id = row.uuid
            active = fake.boolean(chance_of_getting_true=100)
            retention_duration = random.randint(1,266400)
            services = services.append({'uuid':uuid, 'name':name , 'client_id':client_id, 'active': active,'retention_duration':retention_duration},ignore_index=True)
            print(services)
        # services['id'] = services.index
        #print(applications.head())
        # save data to csv
        # services.to_csv("scripts/csv/dump/service.csv", sep=',', encoding='utf-8', index=False)
        services.to_sql("service", con=engine, if_exists="append", chunksize=100, index=False)
        table_df = pd.read_sql_table('service',con=engine)
        table_df.to_csv("scripts/csv/dump/service.csv", sep=',', encoding='utf-8', index=False)


    def service_configuration(self):
        service_configuration = pd.DataFrame(
            columns = ['service_uuid', 'waiting_msg_id', 
            'warning_msg_id', 'welcome_msg_id', 'incall_msg_id', 
            'outcall_msg_id', 'voicemail_msg_id', 'pincode_msg_id', 
            'pincode_sms_content', 'link_confirm_msg_id', 
            'click_to_call_confirm_msg_id', 'click_to_call_failure_msg_id', 
            'pickup_inbound_call', 'pickup_outbound_call', 
            'inbound_call_flow_type', 'active', 'country_code', 
            'inbound_presented_uri_type', 'external_numbers_allowed', 
            'inbound_ringing_timeout', 'outbound_ringing_timeout', 
            'inbound_pai_type', 'outbound_pai_type', 
            'pincode_validation_max_tries', 'pincode_validation_max_delay', 
            'add_diversion_header', 'max_contexts', 'voicemail_group_uuid'])

        mydataframe = pd.read_csv("scripts/csv/dump/service.csv", usecols=[1])

        for row in mydataframe.itertuples():
            service_uuid = row.uuid
            waiting_msg_id= '1'
            warning_msg_id= '1'
            welcome_msg_id= '1'
            incall_msg_id= '1'
            outcall_msg_id= '1'
            voicemail_msg_id= '1'
            pincode_msg_id= '1'
            pincode_sms_content= '1'
            link_confirm_msg_id= '1'
            click_to_call_confirm_msg_id= '1'
            click_to_call_failure_msg_id= '1'
            pickup_inbound_call= fake.boolean(chance_of_getting_true=0)
            pickup_outbound_call= fake.boolean(chance_of_getting_true=0)
            inbound_call_flow_type= str(random.choice(['DIRECT', 'TWO_STEPS', 'API_DRIVEN']))
            active= fake.boolean(chance_of_getting_true=95)
            country_code= 'FR'
            inbound_presented_uri_type= str(random.choice(['PLATFORM','CALLER']))
            external_numbers_allowed= fake.boolean(chance_of_getting_true=95)
            inbound_ringing_timeout= '1'
            outbound_ringing_timeout= '1'
            inbound_pai_type= str(random.choice(['PLATFORM','CALLER','USER','ALIAS']))
            outbound_pai_type= str(random.choice(['PLATFORM','CALLER','USER','ALIAS']))
            pincode_validation_max_tries= '1'
            pincode_validation_max_delay= '1'
            add_diversion_header = fake.boolean(chance_of_getting_true=90)
            max_contexts         = '1'
            voicemail_group_uuid = fake.uuid4()
            service_configuration = service_configuration.append({'service_uuid': service_uuid, 'waiting_msg_id': waiting_msg_id, 'warning_msg_id': warning_msg_id, 'welcome_msg_id': welcome_msg_id, 'incall_msg_id': incall_msg_id, 'outcall_msg_id':outcall_msg_id, 'voicemail_msg_id':voicemail_msg_id, 'pincode_msg_id': pincode_msg_id, 'pincode_sms_content':pincode_sms_content, 'link_confirm_msg_id':link_confirm_msg_id, 'click_to_call_confirm_msg_id':click_to_call_confirm_msg_id, 'click_to_call_failure_msg_id':click_to_call_failure_msg_id, 'pickup_inbound_call':pickup_inbound_call, 'pickup_outbound_call':pickup_outbound_call, 'inbound_call_flow_type':inbound_call_flow_type, 'active':active, 'country_code': country_code, 'inbound_presented_uri_type': inbound_presented_uri_type, 'external_numbers_allowed': external_numbers_allowed, 'inbound_ringing_timeout': inbound_ringing_timeout, 'outbound_ringing_timeout': outbound_ringing_timeout, 'inbound_pai_type': inbound_pai_type, 'outbound_pai_type':outbound_pai_type, 'pincode_validation_max_tries': pincode_validation_max_tries, 'pincode_validation_max_delay' : pincode_validation_max_delay, 'add_diversion_header': add_diversion_header, 'max_contexts': max_contexts, 'voicemail_group_uuid': voicemail_group_uuid},ignore_index=True)
        # add id column
        # service_configuration['id'] = service_configuration.index
        # sort table columns
        service_configuration = service_configuration[['service_uuid', 'waiting_msg_id', 
            'warning_msg_id', 'welcome_msg_id', 'incall_msg_id', 'outcall_msg_id', 
            'voicemail_msg_id', 'pincode_msg_id', 'pincode_sms_content', 
            'link_confirm_msg_id', 'click_to_call_confirm_msg_id', 
            'click_to_call_failure_msg_id', 'pickup_inbound_call', 
            'pickup_outbound_call', 'inbound_call_flow_type', 'active', 'country_code', 
            'inbound_presented_uri_type', 'external_numbers_allowed', 
            'inbound_ringing_timeout', 'outbound_ringing_timeout', 
            'inbound_pai_type', 'outbound_pai_type', 
            'pincode_validation_max_tries', 'pincode_validation_max_delay', 
            'add_diversion_header', 'max_contexts', 'voicemail_group_uuid']]

        print(service_configuration)
        #service_configuration.to_csv("scripts/csv/dump/service_configuration.csv", sep=',', encoding='utf-8', index=False)
        service_configuration.to_sql("service_configuration", con=engine, if_exists="append", chunksize=100, index=False)
        table_df = pd.read_sql_table('service_configuration',con=engine)
        table_df.to_csv("scripts/csv/dump/service_configuration.csv", sep=',', encoding='utf-8', index=False)

    def service_alias_country_whitelist(self):
        service_alias_country_whitelist = pd.DataFrame(columns = ['srv_config_id', 'virtual_number_country', 'user_number_country'])
        mydataframe = pd.read_csv("scripts/csv/dump/service_configuration.csv", usecols=[0])
        for row in mydataframe.itertuples():
            srv_config_id = row.id
            virtual_number_country = "FR"
            user_number_country = "FR"
            service_alias_country_whitelist = service_alias_country_whitelist.append({'srv_config_id':srv_config_id, 'virtual_number_country':virtual_number_country , 'user_number_country':user_number_country},ignore_index=True)
        print(service_alias_country_whitelist)
        service_alias_country_whitelist = service_alias_country_whitelist[['srv_config_id', 'virtual_number_country', 'user_number_country']]
        service_alias_country_whitelist.to_csv("scripts/csv/dump/service_alias_country_whitelist.csv", sep=',', encoding='utf-8', index=True, index_label='id')
        service_alias_country_whitelist.to_sql("service_alias_country_whitelist", con=engine, if_exists="append", chunksize=100,index=False)

    def sms_configuration(self):
        sms_configuration = pd.DataFrame(columns = ['id','sms_enabled', 'sms_default_failure_message'])
        mydataframe = pd.read_csv("scripts/csv/dump/service_configuration.csv", usecols=[0])
        for row in mydataframe.itertuples():
            id = row.id
            sms_enabled = fake.boolean(chance_of_getting_true=90)
            sms_default_failure_message = "Nous n'avons pas pu traiter votre SMS. Nous en sommes desoles."
            sms_configuration = sms_configuration.append({'id':id, 'sms_enabled':sms_enabled , 'sms_default_failure_message':sms_default_failure_message},ignore_index=True)
        print(sms_configuration)
        sms_configuration = sms_configuration[['id', 'sms_enabled', 'sms_default_failure_message']]
        sms_configuration.to_csv("scripts/csv/dump/sms_configuration.csv", sep=',', encoding='utf-8', index=True, index_label='id')
        sms_configuration.to_sql("sms_configuration", con=engine, if_exists="append", chunksize=100,index=False)

    def country(self):
        countries = pd.DataFrame(columns = ['country_code'])
        #mydataframe = pd.read_csv("scripts/csv/dump/service.csv", usecols=[1])
        #number_of_rows = len(mydataframe.index)
        #print(number_of_rows)
        country_list = ['HR','GH','GY','LK','SZ','IQ','NI','MW','UY','AU','FI','LU','SR','SK','ZM','FR','AT','CA','UA','GT','DK','BD','MN','IT','AL','SL','SA','MD','BW','NG','NZ','TN','BH','BR','PA','IL','AF','AR','NL','CU','FJ','MY','SE','NA','CO','NP','BZ','YE','KW','CL','PH','BE','PT','IS','NO','UG','ES','EC','ZA','GB','LV','PK','SI','CZ','KE','LT','DE','BG','JO','QA','GE','SG','IN','MU','HU','PE','VN','MT','MX','IE','ID','MA','JP','US','CY','TH','ZW','GM','KR','MM','TR','MR']
        for idx in country_list:
            countries = countries.append({'country_code': idx},ignore_index=True)
        #print(countries.head())
        countries.to_csv("scripts/csv/dump/countries.csv", sep=',', encoding='utf-8', index=False)
        countries.to_sql("country", con=engine, if_exists="append", chunksize=100,index=False)

    def service_os_configuration(self):
        service_os_configuration = pd.DataFrame(columns = ['service_config_id', 'device_os', 'inbound_presented_uri_type'])
        mydataframe = pd.read_csv("scripts/csv/dump/service_configuration.csv", usecols=[0])
        for row in mydataframe.itertuples():
            service_config_id = row.id
            device_os= str(random.choice(['apple', 'android', 'huawei']))
            inbound_presented_uri_type= '1'
            service_os_configuration = service_os_configuration.append({'service_config_id': service_config_id, 'device_os': device_os, 'inbound_presented_uri_type': inbound_presented_uri_type},ignore_index=True)

        print(service_os_configuration)
        service_os_configuration.to_csv("scripts/csv/dump/service_os_configuration.csv", sep=',', encoding='utf-8', index=False)
        service_os_configuration.to_sql("service_os_configuration", con=engine, if_exists="append", chunksize=100,index=False)

    def service_outbound_country_whitelist(self):
        service_outbound_country_whitelist = pd.DataFrame(columns = ['srv_config_id', 'country_code'])
        mydataframe = pd.read_csv("scripts/csv/dump/service_configuration.csv", usecols=[0])
        for row in mydataframe.itertuples():
            service_config_id = row.id
            country_code= 'FR'
            service_outbound_country_whitelist = service_outbound_country_whitelist.append({'srv_config_id': service_config_id, 'country_code': country_code},ignore_index=True)

        print(service_outbound_country_whitelist)
        service_outbound_country_whitelist.to_csv("scripts/csv/dump/service_outbound_country_whitelist.csv", sep=',', encoding='utf-8', index=False)
        service_outbound_country_whitelist.to_sql("service_outbound_country_whitelist", con=engine, if_exists="append", chunksize=100,index=False)

    def tts_speak(self):
        tts_speak = pd.DataFrame(columns = ['message_id', 'speak_order','voice','language','gender','message'])
        mydataframe = pd.read_csv("scripts/csv/dump/service.csv", usecols=[1])
        message_id = 0
        for row in mydataframe.itertuples():
            message_id = message_id+1
            speak_order= str(random.choice([1, 2, 3]))
            voice = str(random.choice(['1', '2', '3']))
            language = str(random.choice(['FR', 'ES', 'EN']))
            gender = str(random.choice(['MALE', 'FEMALE']))
            message = fake.sentences(nb=1)[0]
            tts_speak = tts_speak.append({'message_id': message_id, 'speak_order': speak_order, 'voice':voice, 'language':language, 'gender':gender, 'message':message },ignore_index=True)

        # tts_speak['id'] = tts_speak.index
        tts_speak = tts_speak[['message_id','speak_order','voice','language','gender','message']]
        print(tts_speak)
        tts_speak.to_csv("scripts/csv/dump/tts_speak.csv", sep=',', encoding='utf-8', index=False)
        tts_speak.to_sql("tts_speak", con=engine, if_exists="append", chunksize=100,index=False)

    def tts_message(self):
        tts_message = pd.DataFrame(columns = ['id'])
        mydataframe = pd.read_csv("scripts/csv/dump/service.csv", usecols=[1])
        message_id = 0
        for row in mydataframe.itertuples():
            message_id += 1
            tts_message = tts_message.append({'id': message_id},ignore_index=True)
        print(tts_message)
        tts_message.to_csv("scripts/csv/dump/tts_message.csv", sep=',', encoding='utf-8', index=False)
        tts_message.to_sql("tts_message", con=engine, if_exists="append", chunksize=100,index=False)