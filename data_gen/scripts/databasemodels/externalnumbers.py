import logging, uuid, random, datetime
import pandas as pd
from random import randint    
from faker import Faker
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from ast import literal_eval
from .helper import *


fake = Faker('fr_FR')
EXTERNALNUMBERS_DBParams = 'mysql://root:root@localhost:3307/numbers_performance'
engine = create_engine(EXTERNALNUMBERS_DBParams, echo=False)
insp = reflection.Inspector.from_engine(engine)



class DatabaseNumbers(): 
    def __init__(self,NO_EXTERNALNUMBERS):
        self.NO_EXTERNALNUMBERS=NO_EXTERNALNUMBERS
        self.clean_db()
        print("Generating numbers db data")
        self.external_number()
        self.external_number_whitelist()

    def clean_db(self):
        with engine.connect() as con:
            data = ['external_number_whitelist','external_number']
            for line in data:
                print("""DELETE FROM numbers_performance."""+line)
                con.execute("""DELETE FROM numbers_performance."""+line)

    def external_number(self):
        numbers = pd.DataFrame(columns = ['number', 'service_config_id','shareable','start_date','end_date','status', 'private_outbound_calls', 'capacity',
                                          'note','voicemail_type','inbound_messages_interface','outbound_messages_interface','sms_http_base_path'])
        apps_df = pd.read_csv("scripts/csv/dump/service.csv", usecols=[1])
        for row in apps_df.itertuples():
            number = fake_french_numbers(8)
            service_config_id = row.uuid
            shareable = fake.boolean(chance_of_getting_true=100)
            start_date = datetime.datetime.now()
            end_date = None # to make all numbers active 
            status = 'ACTIVE'
            private_outbound_calls = fake.boolean(chance_of_getting_true=50) 
            capacity = str(random.choice(['CALL','SMS']))
            note = fake.paragraph()
            voicemail_type = str(random.choice(['userNumber', 'aliasNumber']))
            inbound_messages_interface = 'CEO-SMS'
            outbound_messages_interface = 'CEO-SMS'
            sms_http_base_path = str(random.choice(['http://www.alias.com','http://www.google.com',None]))
            numbers = numbers.append({'number':number, 'service_config_id':service_config_id , 
                'shareable':shareable, 'start_date': start_date,'end_date':end_date,
                'status':status, 'private_outbound_calls':private_outbound_calls, 'capacity':capacity,
                'note':note,'inbound_messages_interface':inbound_messages_interface,'voicemail_type':voicemail_type,
                'outbound_messages_interface':outbound_messages_interface,'sms_http_base_path':sms_http_base_path},ignore_index=True)
            print(numbers)
        numbers = numbers[['number', 'service_config_id','shareable','start_date','end_date','status', 'private_outbound_calls', 'capacity',
                           'note','inbound_messages_interface','outbound_messages_interface','sms_http_base_path']]
        # save data to csv
        numbers.to_csv("scripts/csv/dump/numbers.csv", sep=',', encoding='utf-8', index=False)
        numbers.to_sql("external_number", con=engine, if_exists="append", chunksize=100, index=False)
        table_df = pd.read_sql_table('external_number',con=engine)
        table_df.to_csv("scripts/csv/dump/numbers.csv", sep=',', encoding='utf-8', index=False)

    def external_number_whitelist(self):
        external_number_whitelist = pd.DataFrame(columns = ['number_id','user_number','start_date','end_date','active'])
        apps_df = pd.read_csv("scripts/csv/dump/numbers.csv", usecols=[0])
        for row in apps_df.itertuples():
            number_id  = row.id
            user_number = fake_french_numbers(8)
            start_date = datetime.datetime.now()
            end_date = None
            active = fake.boolean(chance_of_getting_true=100) 
            external_number_whitelist = external_number_whitelist.append({'number_id':number_id,'user_number':user_number , 
                'start_date': start_date,'end_date':end_date, 'active':active},ignore_index=True)
            print(external_number_whitelist)
        # save data to csv
        external_number_whitelist.to_csv("scripts/csv/dump/external_number_whitelist.csv", sep=',', encoding='utf-8', index=False)
        external_number_whitelist.to_sql("external_number_whitelist", con=engine, if_exists="append", chunksize=100, index=False)