from databasemodels.clientsdb import DatabaseClients
from databasemodels.servicesdb import DatabaseServices
from databasemodels.externalnumbers import DatabaseNumbers
from databasemodels.contextsdb import DatabaseContexts
from databasemodels.callsdb import DatabaseCalls
from databasemodels.smsdb import DatabaseSMS
from databasemodels.helper import *


from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

NO_CLIENTS = 5
NO_APPS = 5
NO_SERVICES = 5
NO_EXTERNALNUMBERS = 5
NO_CONTEXTS = 5
NO_Routes = 5
NO_CALLS = 5
NO_SMS = 5


DBParams = 'mysql://root:root@127.0.0.1:3307'
CLIENTS_DBParams = 'mysql://root:root@127.0.0.1:3307/clients_performance'
CALLS_DBParams = 'mysql://root:root@127.0.0.1:3307/calls_performance'
SERVICES_DBParams = 'mysql://root:root@localhost:3307/services_performance'
EXTERNALNUMBERS_DBParams = 'mysql://root:root@localhost:3307/numbers_performance'
CONTEXTS_DBParams = 'mysql://root:root@localhost:3307/contexts_performance'
SMS_DBParams = 'mysql://root:root@127.0.0.1:3307/sms_performance'



def seed():
    DatabaseClients(NO_CLIENTS,NO_APPS)
    DatabaseServices(NO_SERVICES)
    DatabaseNumbers(NO_EXTERNALNUMBERS)
    DatabaseContexts(NO_CONTEXTS)
    DatabaseCalls(NO_Routes,NO_CALLS)
    DatabaseSMS(NO_SMS)

def harvest():
    print("to do")

if __name__ == "__main__":
    seed()
    # harvest()