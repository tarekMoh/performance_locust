﻿/* click2call*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid,
  uctxt.uuid AS contextid
FROM contexts_performance.usage_context uctxt,
     services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps
WHERE uctxt.service_config_uuid = srv.uuid
AND clints.uuid = srv.client_id
AND uctxt.pincode_status = "ACTIVATED"
AND apps.client_id = clints.id;

/* create_routes*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid,
  SUBSTRING(contexts.alias_number,2) AS extrernalNum,
  enwhitelist.user_number AS usernumber,
  extnumbers.note AS note
FROM services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps,
     contexts_performance.usage_context contexts,
     numbers_performance.external_number extnumbers,
     numbers_performance.external_number_whitelist enwhitelist
WHERE clints.uuid = srv.client_id
AND extnumbers.service_config_id = srv.uuid
AND apps.client_id = clints.id
AND enwhitelist.number_id = extnumbers.id
AND contexts.service_config_uuid=srv.uuid
AND contexts.alias_number=extnumbers.number;


/* get_contexts*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid,
  REPLACE(uctxt.user_number, '+','') AS usernumber,
  REPLACE(uctxt.alias_number, '+','')  AS aliasnumber,
  'ACTIVATED' AS status,
  strftime('%Y-%m-%dT%H:%M:%S.0Z',creationdate) AS creationdate,
  strftime('%Y-%m-%dT%H:%M:%S.0Z',expirationdate)  AS expirationdate,
  '0' AS 'offset',
  '100' AS 'limit',
  ELT(0.5 + RAND() * 6, 'id', 'aliasNumber', 'userNumber', 'creationDate', 'expirationDate', 'status') AS sort

FROM contexts_performance.usage_context uctxt,
     services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps,
     numbers_performance.external_number extnumbers
WHERE uctxt.service_config_uuid = srv.uuid
AND clints.uuid = srv.client_id
AND uctxt.pincode_status = "ACTIVATED"
AND extnumbers.service_config_id = srv.uuid
AND apps.client_id = clints.ID;

/* get_external_numbers*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid

FROM services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps,
     numbers_performance.external_number extnumbers
WHERE clints.uuid = srv.client_id
AND extnumbers.service_config_id = srv.uuid
AND apps.client_id = clints.id;
/* *********** */

/* get_routes*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid,
  '2021-01-01T00:00:00.000Z' AS 'startdate',
  '3021-01-01T00:00:00.000Z' AS 'enddate',
  ELT(0.5 + RAND() * 2, 'true', 'false') AS status,
  JSON_EXTRACT(uctxt.note, "$.aliasName") AS username,
  '0' AS 'offset',
  '100' AS 'limit'
  
FROM contexts_performance.usage_context uctxt,
     services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps
WHERE uctxt.service_config_uuid = srv.uuid
AND clints.uuid = srv.client_id
AND apps.client_id = clints.id;
FROM services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps
WHERE clints.uuid = srv.client_id
AND apps.client_id = clints.ID;

/*outboundcalls*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid,
  uctxt.uuid AS contextid,
  RPAD(CONCAT('33', CAST(RAND() * 100000000 AS decimal)), 11, 0) AS customernumber
FROM contexts_performance.usage_context uctxt,
     services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps
WHERE uctxt.service_config_uuid = srv.uuid
AND clints.uuid = srv.client_id
AND uctxt.pincode_status = "ACTIVATED"
AND apps.client_id = clints.id;

/*create routes*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid,
  uctxt.user_number AS usernumber,
  REPLACE(uctxt.alias_number, '+','')  AS aliasnumber,
  LEFT(MD5(RAND()), 1000) AS note
FROM contexts_performance.usage_context uctxt,
     services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps
WHERE uctxt.service_config_uuid = srv.uuid
AND clints.uuid = srv.client_id
AND uctxt.pincode_status = "ACTIVATED"
AND apps.client_id = clints.id;

/*get services*/
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid
FROM services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps
WHERE clints.uuid = srv.client_id
AND apps.client_id = clints.id;





SELECT
       apps.app_id                        AS appid
     , srv.uuid                           AS serviceid
     , SUBSTRING(contexts.alias_number,2) AS extrernalNum
     , enwhitelist.user_number            AS usernumber
     , extnumbers.note                    AS note
FROM
       services_performance.service                  srv
     , clients_performance.client                    clints
     , clients_performance.application               apps
     , contexts_performance.usage_context            contexts
     , numbers_performance.external_number           extnumbers
     , numbers_performance.external_number_whitelist enwhitelist
WHERE
       clints.uuid                      = srv.client_id
       AND extnumbers.service_config_id = srv.uuid
       AND apps.client_id               = clints.id
       AND enwhitelist.number_id        = extnumbers.id
       AND contexts.service_config_uuid =srv.uuid
       AND contexts.alias_number        =extnumbers.number;

select
       appid                                           as appid, servid                                          as servid, no1                                             as no1, no2                                             as no2, 'ACTIVATED'                                     as status,
       strftime('%Y-%m-%dT%H:%M:%S.0Z',creationdate)   as creationdate,
       strftime('%Y-%m-%dT%H:%M:%S.0Z',expirationdate) as expirationdate,
 offset as offset,
 [limit] as [limit],
 sort as sort
from get_contexts;






/* e2e */
/* click2call + outboundcall */
SELECT
  apps.app_id AS appid,
  srv.uuid AS serviceid,
  uctxt.UUID AS ctxid,
  REPLACE(uctxt.alias_number, '+','')  AS aliasnumber,
  REPLACE(uctxt.user_number, '+','') AS usernumber
FROM contexts_performance.usage_context uctxt,
     services_performance.service srv,
     clients_performance.client clints,
     clients_performance.application apps,
     numbers_performance.external_number extnumbers
WHERE uctxt.service_config_uuid = srv.uuid
AND clints.uuid = srv.client_id
AND uctxt.pincode_status = "ACTIVATED"
AND extnumbers.service_config_id = srv.uuid
AND apps.client_id = clints.ID;